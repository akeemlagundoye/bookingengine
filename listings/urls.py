from posixpath import basename
from django.urls import path

from listings.views import BookingInfoSerializer, BookingInfoAPIView

app_name = 'listings'

urlpatterns = [
    path('units/', BookingInfoAPIView.as_view(), name='booking_info')
]
