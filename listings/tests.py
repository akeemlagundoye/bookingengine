import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from .models import BookedDays, BookingInfo, Listing

# Helper function for creating a listing
def create_listing(listing_type, title, country, city):
        return Listing.objects.create(
            listing_type=listing_type,
            title=title,
            country=country,
            city=city
        )

# Helper function for creating a bookedDay
def create_booking_list(price):
    listing = create_listing("Apartment", "Luxurious Studio", "England", "Birmingham")

    booking_info = BookingInfo(listing=listing, price=price)
    booking_info.save()

    check_in = timezone.now() + datetime.timedelta(days=1)
    check_out = timezone.now() + datetime.timedelta(days=5)

    return BookedDays.objects.create(booking_info=booking_info, check_in=check_in, check_out=check_out)
    


class ListingTests(TestCase):
    def test_create_listing(self):
        listing = create_listing(
                    "Apartment",
                    "Luxurious Studio",
                    "Norway",
                    "Oslo"
                    )
        self.assertTrue(isinstance(listing, Listing))


LIST_BOOKING_URL = reverse('listings:booking_info')
class BookingInfoTestCase(APITestCase):

    def setUp(self):
        self.client = APIClient()
        # Creating a list of price to pupulate our list
        price_list = [50, 60, 200]

        for price in price_list:
            self.booking_info = create_booking_list(price)

    def test_get_booking_list(self):
        url = LIST_BOOKING_URL
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 200)
        
        self.assertIsInstance(response.data, list)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_below_max_fifty(self):
        url = LIST_BOOKING_URL
        response = self.client.get(url)

        max_price = response.data[0]['price']
        blocked_day = create_booking_list(price=max_price)
        booking_info_qs = BookingInfo.objects.filter(price__lte=max_price)
        
        if booking_info_qs and blocked_day:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
        
    def test_below_max_sixty(self):
        url = LIST_BOOKING_URL
        response = self.client.get(url)
        

        max_price = response.data[1]['price']
        blocked_day = create_booking_list(price=max_price)
        booking_info_qs = BookingInfo.objects.filter(price__lte=max_price)
        
        if booking_info_qs and blocked_day:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)

    def test_above_two_hundred(self):
        url = LIST_BOOKING_URL
        response = self.client.get(url)
        
        max_price = response.data[2]['price']
        booking_info_qs = BookingInfo.objects.filter(price__lte=max_price)

        if booking_info_qs:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
