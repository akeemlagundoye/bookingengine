from django.db.models import Q
from django.shortcuts import render
from rest_framework import generics

from .models import BookedDays, BookingInfo
from .serializers import BookedDaysSerializer, BookingInfoSerializer


class BookingInfoAPIView(generics.ListAPIView):
    """
    A ListAPIView function to filter our max prices and also the booked days
    """
    serializer_class = BookingInfoSerializer

    def get_queryset(self):
        max_price = self.request.query_params.get('max_price')
        check_in = self.request.query_params.get('check_in')
        check_out = self.request.query_params.get('check_out')
        
        queryset = BookingInfo.objects.all()

        # filter out price lesser than or equal to the max_price specified
        if max_price:
            queryset = queryset.filter(price__lte=max_price)
        
        # Use Django Q object to filter out booked days
        if check_in and check_out:
            booked_listing = BookedDays.objects.filter(Q(check_in__lte=check_in,
                check_out__gte=check_in) | Q(check_in__lte=check_out, check_out__gte=check_out))
            
            queryset = queryset.exclude(id__in=[item.booking_info.id for item in booked_listing])

        # Ordering our queryset in price from lowest to highest
        return queryset.order_by('price')


class BookedDayAPIView(generics.ListCreateAPIView):
    queryset = BookedDays.objects.all()
    serializer_class = BookedDaysSerializer
