from rest_framework import serializers

from listings.models import Listing, BookingInfo, BookedDays


class ListingSerializer(serializers.ModelSerializer):
    """
    Serializing all the fields in the Listing Model to return a JSON
    """
    class Meta:
        model = Listing
        fields = '__all__'


class BookingInfoSerializer(serializers.ModelSerializer):
    """
    Serializing the BookingInfo into a JSON with all the fields
    """
    class Meta:
        model = BookingInfo
        fields = ('listing_type', 'title', 'country', 'city', 'price')


class BookedDaysSerializer(serializers.ModelSerializer):
    """
    Serializing the attributes of BookedDays
    """
    booking_info = BookingInfoSerializer(many=True)
    class Meta:
        model = BookedDays
        fields = ('booking_info', 'check_in', 'check_out')
